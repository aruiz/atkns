#include <gmodule.h>
#include <gtk/gtk.h>
#include <gdk/gdkquartz.h>
#include <glib.h>
#include "atkns.h"

GHashTable *atknsmap;

@implementation AtkNsObject
- (id)init:(AtkObject *)atko withParent:(NSAccessibilityElement*)parent{
  self->atkobj = atko;
  return self;
 }
@end

@implementation AtkNsComponent
- (nullable id)init:(AtkObject *)atko withParent:(NSAccessibilityElement *)parent {
  if (!ATK_IS_COMPONENT(atko)) {
    return NULL;
  }
  self->atkobj = atko;
  return self;
}
- (NSRect)accessibilityFrame {
  gint x, y, w, h;
  atk_component_get_extents(ATK_COMPONENT(self->atkobj), &x, &y, &w, &h, ATK_XY_SCREEN);

  NSRect ret;

  ret.origin.x = x;
  ret.origin.y = y;
  ret.size.height = h;
  ret.size.width = w;

  return ret;
}

- (nullable id)accessibilityParent {
  AtkObject *parent = atk_object_get_parent(self->atkobj);
  return g_hash_table_lookup(atknsmap, parent);
}

- (NSString *)accessibilityIdentifier {
  const char *name = atk_object_get_name(self->atkobj);
  NSString *ns_path = [[NSString string] initWithUTF8String:name];
  return ns_path;
}
@end

static void
foreach_child(AtkObject *root, NSAccessibilityElement *rootnsa) {
  if (ATK_IS_COMPONENT(root)) {

    AtkNsComponent *comp = [[AtkNsComponent alloc] init:root withParent:rootnsa];
    [rootnsa accessibilityAddChildElement:comp];
  }
  g_warning("%s", atk_role_get_name(atk_object_get_role(root)));
  gint roota_n_children = atk_object_get_n_accessible_children(root);
  for(gint j=0;j<roota_n_children; j++) {
    AtkObject *child = atk_object_ref_accessible_child (root, j);
    foreach_child(child, NULL);
    g_object_unref(child);
  }
}

static gboolean
introspect_atk_root (gpointer data) {
  //g_hash_table_remove_all (atknsmap);

  GList *toplevels = gtk_window_list_toplevels();
  const guint size = g_list_length(toplevels);
  for (guint i = 0; i<size; i++ ) {
    GtkWindow *win = GTK_WINDOW(g_list_nth_data(toplevels, i));
    GdkWindow *gdkwin = gtk_widget_get_window(GTK_WIDGET(win));
    NSWindow *nsw = gdk_quartz_window_get_nswindow(gdkwin);
    AtkObject *atkwin = gtk_widget_get_accessible(GTK_WIDGET(win));

    foreach_child(atkwin, (NSAccessibilityElement*)nsw);
  }
  if (size == 0) {
    g_warning("No root ATK Object");
    return TRUE;
  }
  return FALSE;
}

int
gtk_module_init (gint * argc, gchar ** argv[])
{
    atknsmap = g_hash_table_new(g_direct_hash, g_direct_equal);
    g_timeout_add(1000, introspect_atk_root, NULL);
    return 0;
}

gchar*
g_module_check_init (GModule *module)
{
  g_module_make_resident (module);

  return NULL;
}