#include <AppKit/AppKit.h>
#include <atk/atk.h>

@interface AtkNsObject : NSAccessibilityElement {
    AtkObject *atkobj;
}
-(id)init:(AtkObject *)atko withParent:(NSAccessibilityElement*)parent;
@end

@interface AtkNsComponent : AtkNsObject
@end